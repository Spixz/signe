from flask import Flask, request, render_template, render_template_string, jsonify
from flask_mysqldb import MySQL

class CustomFlask(Flask):
    jinja_options = Flask.jinja_options.copy()
    jinja_options.update(dict(
        block_start_string='DD',
        block_end_string='DDE',
        variable_start_string='{{{',
        variable_end_string='}}}',
        comment_start_string='RF',
        comment_end_string='BG',
        ))

app = CustomFlask(__name__)

app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'phpmyadmin2'
app.config['MYSQL_PASSWORD'] = 'zeda'
app.config['MYSQL_DB'] = 'lsf'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)


@app.route("/")
def home():
    return render_template('index.html')

@app.route("/course/<int:id>")
def course(id):
    return render_template('course.html', id=id)

@app.route("/add_course", methods=['GET', 'POST'])
def add_course():
    if (request.method == 'GET'):
       return render_template('add_course.html') 
    else:
        cur = mysql.connection.cursor()        
        title = request.form['title']
        words = request.form['words'].replace('\r', '').split('\n')

        cur.execute("INSERT INTO courses (title) VALUES (%s)", [title])
        mysql.connection.commit()
        cur.execute("SELECT id FROM courses WHERE title = '"+title+"'")
        id_course = cur.fetchone()['id']

        words_for_insert = []
        for word in words:
            sepatate = word.split(';')
            words_for_insert.append([sepatate[0], sepatate[1]])
        cur.executemany("INSERT INTO words (word, video_link) VALUES (%s, %s)", words_for_insert)
        mysql.connection.commit()

        word_of_courses = []
        for word in words:
            word_of_courses.append([id_course, word.split(';')[1]])
        cur.executemany("INSERT INTO word_of_courses (id_course, video_link) VALUES (%s, %s)", word_of_courses) 
        mysql.connection.commit()
    return ("POST")

@app.route("/api/<path>")
@app.route("/api/<path>/<int:id>")
def api(path, id=0):
    request = ''
    cur = mysql.connection.cursor()
    if (path == "all_courses"):
        cur.execute('SELECT * FROM words')
    elif (path == "course" and id > 0):
        cur.execute("SELECT words.word, words.video_link FROM words, word_of_courses WHERE word_of_courses.id_course = "+str(id)+" AND words.video_link = word_of_courses.video_link")
    elif (path == "course_title" and id > 0):
        cur.execute("SELECT title FROM courses WHERE id = "+str(id))
    else:
        return ('bad request')
    rv = cur.fetchall()
    return(jsonify(rv))

# if __name__ == "__main__":
#     app.run()